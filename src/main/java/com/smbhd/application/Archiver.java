package com.smbhd.application;

import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component(value = "Archiver")
public class Archiver {

    @Autowired
    private SalesforceService salesforce;
    private String targetDirectory = null;
    private Calendar startDate = null;
    private Calendar endDate = null;
    private List<SObject> errors = new ArrayList<>();
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
    private Map<String, SObject> linksByDocumentId = new HashMap<>();

    public void writeFilesToDirectory(Map<DoclinkImport, byte[]> docsWithBody) throws JAXBException, ConnectionException {
        JAXBContext xmlContext = JAXBContext.newInstance(DoclinkImport.class);
        Marshaller xmlConverter = xmlContext.createMarshaller();

        for (DoclinkImport doclinkImport : docsWithBody.keySet()) {
            try {
                xmlConverter.marshal(doclinkImport, new FileOutputStream(targetDirectory + doclinkImport.getFileName() + ".xml"));
                Files.write(Paths.get(targetDirectory + doclinkImport.getDocuments().getDocument().getImages().getImage()), docsWithBody.get(doclinkImport));
            } catch (IOException e) {
                SObject error = new SObject("Archive_Error__c");
                error.setField("Document_Id__c", doclinkImport.getId());
                error.setField("Document_Name__c", doclinkImport.getDocuments().getDocument().getImages().getImage());
                error.setField("Error_Date__c", Calendar.getInstance());
                error.setField("Message__c", e.getMessage());
                errors.add(error);
            }
        }
        if (errors.size() > 0) {
            System.err.println("There were errors while writing files! Make sure to check Salesforce Archive Error object!");
            salesforce.writeErrors(errors);
        }
    }

    public String getSFDocumentType(String name, String object) {
        // Remove extension if any
        if (name.lastIndexOf(".") != -1) {
            name = name.substring(0, name.lastIndexOf("."));
        }
        if (object.equalsIgnoreCase("Opportunity")) {
            if (name.endsWith("RFP")) {
                return "RFP Documents";
            } else if (name.endsWith("PO")) {
                return "Customer PO";
            } else if (name.endsWith("BOM")) {
                return "BOM";
            } else if (name.endsWith("SOW")) {
                return "SOW";
            } else if (name.endsWith("RUS")) {
                return "RUS";
            } else if (name.endsWith("RUSA")) {
                return "Rus Acceptance (signature page)";
            } else if (name.endsWith("CAP")) {
                return "Misc-Cisco Cap";
            } else if (name.endsWith("NDA")) {
                return "NDA";
            } else if (name.endsWith("CO")) {
                return "Change Orders";
            } else {
                return "NULL";
            }
        } else {
            if (name.endsWith("DNBI")) {
                return "D&B Reports";
            } else if (name.endsWith("NDA")) {
                return "NDA";
            } else if (name.endsWith("TAX")) {
                return "Tax Exempt Certifications";
            } else if (name.endsWith("MA")) {
                return "Master Customer Agreements";
            } else {
                return "NULL";
            }
        }
    }

    public Map<DoclinkImport, byte[]> getArchiveDocumentToBodyMap(List<SObject> attachmentsAndFiles) {
        Map<DoclinkImport, byte[]> docsWithBody = new HashMap<>();

        for (SObject attachmentOrFile : attachmentsAndFiles) {
            DoclinkImport docLinkImport = new DoclinkImport();
            docLinkImport.setId((String) attachmentOrFile.getField("Id"));
            byte[] data = null;

            DoclinkImport.DocumentType documentType = new DoclinkImport.DocumentType();
            DoclinkImport.Images images = new DoclinkImport.Images();
            DoclinkImport.Property property1 = new DoclinkImport.Property();
            property1.setType("");
            DoclinkImport.Property property2 = new DoclinkImport.Property();
            property2.setName("SFDocumentType");
            property2.setType("");
            if (attachmentOrFile.getType().equalsIgnoreCase("Attachment")) {
                String extension = (String) attachmentOrFile.getField("ContentType");
                if (extension != null) {
                    docLinkImport.setFileExtension(computeFileExtension(extension));
                } else {
                    docLinkImport.setFileExtension(((String) attachmentOrFile.getField("Name")).substring(((String) attachmentOrFile.getField("Name")).lastIndexOf(".") + 1));
                }
                docLinkImport.setFileName(attachmentOrFile.getField("Id") + " " + attachmentOrFile.getField("Name"));
                images.setImage(docLinkImport.getFileName() + "." + docLinkImport.getFileExtension());
                String docType = (String) attachmentOrFile.getChild("Parent").getField("Type");
                if (docType.equalsIgnoreCase("Account")) {
                    documentType.setName("SF Account Doc");
                    property1.setName("AccountID");
                    property1.setValue((String) attachmentOrFile.getField("ParentId"));
                } else {
                    documentType.setName("SF Opportunity Doc");
                    property1.setName("OpportunityID");
                    property1.setValue((String) attachmentOrFile.getField("ParentId"));
                }
                property2.setValue(getSFDocumentType((String) attachmentOrFile.getField("Name"), docType));
                data = Base64.getDecoder().decode(((String) attachmentOrFile.getField("Body")).getBytes());
            }
            if (attachmentOrFile.getType().equalsIgnoreCase("ContentVersion")) {
                docLinkImport.setFileExtension(computeFileExtension((String) attachmentOrFile.getChild("ContentDocument").getField("FileExtension")));
                docLinkImport.setFileName(attachmentOrFile.getField("Id") + " " + attachmentOrFile.getField("Title"));
                images.setImage(docLinkImport.getFileName() + "." + docLinkImport.getFileExtension());
                SObject documentLink = linksByDocumentId.get((String) attachmentOrFile.getField("ContentDocumentId"));
                if (documentLink == null) {
                    System.out.println("Document with id " + attachmentOrFile.getField("Id") + " not linked to Account or Opportunity!");
                    continue;
                }
                String docType = (String) documentLink.getChild("LinkedEntity").getField("Type");
                if (docType.equalsIgnoreCase("Account")) {
                    documentType.setName("SF Account Doc");
                    property1.setName("AccountID");
                    property1.setValue((String) linksByDocumentId.get((String) attachmentOrFile.getField("ContentDocumentId")).getField("LinkedEntityId"));
                } else {
                    documentType.setName("SF Opportunity Doc");
                    property1.setName("OpportunityID");
                    property1.setValue((String) linksByDocumentId.get((String) attachmentOrFile.getField("ContentDocumentId")).getField("LinkedEntityId"));
                }
                property2.setValue(getSFDocumentType((String) attachmentOrFile.getField("Title"), docType));
                data = Base64.getDecoder().decode(((String) attachmentOrFile.getField("VersionData")).getBytes());
            }
            DoclinkImport.TopLevelFolder topLevelFolder = new DoclinkImport.TopLevelFolder();
            topLevelFolder.setName("CCI");
            DoclinkImport.Properties properties = new DoclinkImport.Properties();
            properties.setProperty1(property1);
            properties.setProperty2(property2);
            DoclinkImport.KeyProperty keyProperty = new DoclinkImport.KeyProperty();
            keyProperty.setName("");
            keyProperty.setValue("");
            keyProperty.setType("");
            DoclinkImport.KeyProperties keyProperties = new DoclinkImport.KeyProperties();
            keyProperties.setKeyProperty(keyProperty);
            DoclinkImport.Actions actions = new DoclinkImport.Actions();
            actions.setAction("");

            DoclinkImport.Document document = new DoclinkImport.Document();
            document.setTopLevelFolder(topLevelFolder);
            document.setDocumentType(documentType);
            document.setImages(images);
            document.setProperties(properties);
            document.setKeyProperties(keyProperties);
            document.setActions(actions);
            DoclinkImport.Documents documents = new DoclinkImport.Documents();
            documents.setDocument(document);
            docLinkImport.setDocuments(documents);

            docsWithBody.put(docLinkImport, data);
        }
        return docsWithBody;
    }

    private String computeFileExtension(String contentType) {
        contentType = contentType.substring(contentType.lastIndexOf('/') + 1);
        if (contentType.startsWith("vnd")) {
            if (contentType.contains("word")) {
                contentType = "docx";
            }
            if (contentType.contains("excel") || contentType.contains("spreadsheet")) {
                contentType = "xlsx";
            }
            if (contentType.contains("powerpoint") || contentType.contains("presentation")) {
                contentType = "pptx";
            }
        }
        return contentType;
    }

    public List<SObject> getAttachments() throws ConnectionException {
        return salesforce.queryAttachments(startDate, endDate);
    }

    public List<SObject> getFiles() throws ConnectionException {
        List<SObject> files = new ArrayList<>();
        files.addAll(salesforce.queryFiles(startDate, endDate));
        linksByDocumentId = salesforce.queryLinks(files);
        return files;
    }

    public void setArgs(String... args) throws ParseException {
        targetDirectory = "./";
        startDate = Calendar.getInstance();
        endDate = Calendar.getInstance();
        endDate.add(Calendar.DATE, 1);
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-s")) {
                startDate.setTime(dateFormat.parse(args[i + 1]));
            }
            if (args[i].equals("-e")) {
                endDate.setTime(dateFormat.parse(args[i + 1]));
            }
            if (args[i].equals("-d")) {
                targetDirectory = args[i + 1];
                if (!targetDirectory.endsWith("/")) {
                    targetDirectory = targetDirectory + "/";
                }
            }
        }
        if (startDate.getTime().getTime() > endDate.getTime().getTime()) {
            throw new ParseException("Start time cannot be greater than end time!", 0);
        }
    }

    public String getTargetDirectory() {
        return targetDirectory;
    }

    public String getStartDateString() {
        return dateFormat.format(startDate.getTime());
    }

    public String getEndDateString() {
        return dateFormat.format(endDate.getTime());
    }
}
