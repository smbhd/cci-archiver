package com.smbhd.application;

import com.sforce.soap.partner.*;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class SalesforceService {

    @Value("${app.errorEmails}")
    private String errorEmails;
    private PartnerConnection salesforce;
    private SimpleDateFormat sfdcDateFormat;

    @Autowired
    public SalesforceService(@Value("${salesforce.userName}") String salesforceUsername, @Value("${salesforce.passwordToken}") String salesforcePasswordToken,
                             @Value("${salesforce.authEndpoint}") String salesforceAuthEndpoint) throws ConnectionException {
        sfdcDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");
        sfdcDateFormat.setTimeZone(TimeZone.getTimeZone("GMT-6"));

        ConnectorConfig salesforceConfig = new ConnectorConfig();
        salesforceConfig.setUsername(salesforceUsername);
        salesforceConfig.setPassword(salesforcePasswordToken);
        salesforceConfig.setAuthEndpoint(salesforceAuthEndpoint);
        salesforce = Connector.newConnection(salesforceConfig);
    }

    public List<SObject> queryAttachments(Calendar startDate, Calendar endDate) throws ConnectionException {
        QueryResult salesforceQuery = salesforce.query("select Id, Name, Body, ParentId, Parent.Type, ContentType from Attachment where (Parent.Type = 'Account' or Parent.Type = 'Opportunity') and LastModifiedDate > "
                + sfdcDateFormat.format(startDate.getTime()) + " and LastModifiedDate < " + sfdcDateFormat.format(endDate.getTime()));
        return getAllResults(salesforceQuery);
    }

    public List<SObject> queryFiles(Calendar startDate, Calendar endDate) throws ConnectionException {
        QueryResult salesforceQuery = salesforce.query("select Id, Title, VersionData, ContentDocumentId, ContentDocument.FileExtension from ContentVersion where IsLatest = true and LastModifiedDate > "
                + sfdcDateFormat.format(startDate.getTime()) + " and LastModifiedDate < " + sfdcDateFormat.format(endDate.getTime()));
        return getAllResults(salesforceQuery);
    }

    private List<SObject> getAllResults(QueryResult salesforceQuery) throws ConnectionException {
        List<SObject> list = new ArrayList<>();
        if (salesforceQuery.getSize() == 0) {
            System.out.println("\r100%");
            return list;
        }
        boolean done = false;
        while(!done) {
            list.addAll(Arrays.asList(salesforceQuery.getRecords()));
            if (salesforceQuery.isDone()) {
                done = true;
            } else {
                salesforceQuery = salesforce.queryMore(salesforceQuery.getQueryLocator());
            }
            System.out.print("\r" + list.size() * 100 / salesforceQuery.getSize() + "%");
        }
        System.out.println();
        return list;
    }

    public Map<String, SObject> queryLinks(List<SObject> files) throws ConnectionException {
        List<String> contentDocumentIds = new ArrayList<>();
        for(SObject file : files) {
            contentDocumentIds.add((String) file.getField("ContentDocumentId"));
        }
        QueryResult salesforceQuery = salesforce.query("Select ContentDocumentId, LinkedEntity.Type, LinkedEntityId from ContentDocumentLink where ContentDocumentId IN ('" + String.join("','", contentDocumentIds) + "')");
        System.out.println("Linking attributes...");
        List<SObject> contentDocumentLinks = getAllResults(salesforceQuery);
        Map<String, SObject> contentDocumentLinksByContentDocumentId = new HashMap<>();
        for (SObject contentDocumentLink : contentDocumentLinks) {
            String docType = (String) contentDocumentLink.getChild("LinkedEntity").getField("Type");
            if (!docType.equalsIgnoreCase("Account") && !docType.equals("Opportunity")) {
                continue;
            }
            contentDocumentLinksByContentDocumentId.put((String) contentDocumentLink.getField("ContentDocumentId"), contentDocumentLink);
        }
        return contentDocumentLinksByContentDocumentId;
    }

    public void writeErrors(List<SObject> errors) throws ConnectionException {
        Arrays.asList(salesforce.create(errors.toArray(new SObject[errors.size()])));

        SingleEmailMessage emailMessage = new SingleEmailMessage();
        emailMessage.setSubject("Archive Errors Occurred!");
        emailMessage.setPlainTextBody("There were errors while writing files! Make sure to check Salesforce Archive Error object to see the specifics!");
        emailMessage.setToAddresses(errorEmails.split(";"));
        salesforce.sendEmail(new Email[] {emailMessage});
    }
}

