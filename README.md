This application connects to a salesforce environment and pulls the attachments and contentDocuments (ChatterDocs) and pulls attributes to write to an xml metadata file. It also writes the source file to a directory.

Attributes that can be customized:  
Via properties file:  
Email addresses to which we send errors.  
Salesforce environment to which we connect.  
Via command line:  
Directory to write files.  
Date from which to query modified files.  

There should be a maximum of 2 arguments. Date and Directory. The date should be in the format '04-14-2014'.  
The directory should start with /, ./, or ../  
If the date is not specified, it will be run Today. If the directory is not specified, it will extract files to current working directory. Note: the directory must exist before writing files.
Usage:

```
#!java

 java -jar cci-archiver.jar
 java -jar cci-archiver.jar 12-25-2016
 java -jar cci-archiver.jar ./mydirectory
 java -jar cci-archiver.jar 12-25-2016 ./mydirectory
 java -jar cci-archiver.jar ./mydirectory/ 12-25-2016
```