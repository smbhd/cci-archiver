package com.smbhd.application;

import org.springframework.stereotype.Component;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@Component
@XmlRootElement(name="DoclinkImport")
public class DoclinkImport {

    private String id;
    private String fileName;
    private String fileExtension;
    private Documents documents;

    @XmlTransient
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @XmlTransient
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @XmlTransient
    public String getFileExtension() {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    @XmlElement(name = "Documents")
    public Documents getDocuments() {
        return documents;
    }

    public void setDocuments(Documents documents) {
        this.documents = documents;
    }

    public static class Documents {
        private Document document;

        @XmlElement(name = "Document")
        public Document getDocument() {
            return document;
        }

        public void setDocument(Document document) {
            this.document = document;
        }
    }

    @XmlType(propOrder = {"topLevelFolder", "documentType", "images", "properties", "keyProperties", "actions"})
    public static class Document {
        private TopLevelFolder topLevelFolder;
        private DocumentType documentType;
        private Images images;
        private Properties properties;
        private KeyProperties keyProperties;
        private Actions actions;

        @XmlElement(name = "TopLevelFolder")
        public TopLevelFolder getTopLevelFolder() {
            return topLevelFolder;
        }

        public void setTopLevelFolder(TopLevelFolder topLevelFolder) {
            this.topLevelFolder = topLevelFolder;
        }

        @XmlElement(name = "DocumentType")
        public DocumentType getDocumentType() {
            return documentType;
        }

        public void setDocumentType(DocumentType documentType) {
            this.documentType = documentType;
        }

        @XmlElement(name = "Images")
        public Images getImages() {
            return images;
        }

        public void setImages(Images images) {
            this.images = images;
        }

        @XmlElement(name = "Properties")
        public Properties getProperties() {
            return properties;
        }

        public void setProperties(Properties properties) {
            this.properties = properties;
        }

        @XmlElement(name = "KeyProperties")
        public KeyProperties getKeyProperties() {
            return keyProperties;
        }

        public void setKeyProperties(KeyProperties keyProperties) {
            this.keyProperties = keyProperties;
        }

        @XmlElement(name = "Actions")
        public Actions getActions() {
            return actions;
        }

        public void setActions(Actions actions) {
            this.actions = actions;
        }
    }

    public static class TopLevelFolder {
        private String name;

        @XmlElement(name = "Name")
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class DocumentType {
        private String name;

        @XmlElement(name = "Name")
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class Images {
        private String image;

        @XmlElement(name = "Image")
        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }

    @XmlType(propOrder = {"property1","property2"})
    public static class Properties {
        private Property property1;
        private Property property2;

        @XmlElement(name = "Property")
        public Property getProperty1() {
            return property1;
        }

        public void setProperty1(Property property1) {
            this.property1 = property1;
        }

        @XmlElement(name = "Property")
        public Property getProperty2() {
            return property2;
        }

        public void setProperty2(Property property2) {
            this.property2 = property2;
        }
    }

    @XmlType(propOrder = {"name", "value", "type"})
    public static class Property {
        private String name;
        private String value;
        private String type;

        @XmlElement(name = "Name")
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @XmlElement(name = "Value")
        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        @XmlElement(name = "Type")
        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public static class KeyProperties {
        private KeyProperty keyProperty;

        @XmlElement(name = "KeyProperty")
        public KeyProperty getKeyProperty() {
            return keyProperty;
        }

        public void setKeyProperty(KeyProperty keyProperty) {
            this.keyProperty = keyProperty;
        }
    }

    @XmlType(propOrder = {"name", "value", "type"})
    public static class KeyProperty {
        private String name;
        private String value;
        private String type;

        @XmlElement(name = "Name")
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @XmlElement(name = "Value")
        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        @XmlElement(name = "Type")
        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public static class Actions {
        private String action;

        @XmlElement(name = "Action")
        public String getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }
    }
}