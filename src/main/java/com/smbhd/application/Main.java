package com.smbhd.application;

import com.sforce.soap.partner.sobject.SObject;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Initializes the spring applicationContext
 */
public class Main {

    /**
     * Initializes the spring applicationContext
     * @param args There should be a maximum of 3 arguments. Start Date, End Date and Directory. The dates should be in the format '04-14-2014'.
     *             The directory should start with /, ./, or ../
     *             If the start date is not specified, it will be run Today.
     *             If the end date is not specified, it will run from start date to Today.
     *             If the end date IS specified but no start date, it will error out.
     *             If the directory is not specified, it will extract files to current working directory.
     *             Start date is defined with a -s flag. End date with a -e flag and directory with a -d flag
     * <pre>Usage:
     * {@code   java -jar cci-archiver.jar
     *          java -jar cci-archiver.jar -s 12-25-2016
     *          java -jar cci-archiver.jar -d ./mydirectory
     *          java -jar cci-archiver.jar -s 12-25-2016 -d ./mydirectory
     *          java -jar cci-archiver.jar -s 12-25-2016 -e 01-01-2016 -d ./mydirectory
     * }</pre>
     */
    public static void main(String... args) {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        Archiver archiver = (Archiver) applicationContext.getBean("Archiver");

        try {
            System.out.println("\nParsing parameters if any.");
            archiver.setArgs(args);

            System.out.println("Querying salesforce for all attachments from " + archiver.getStartDateString() + " to " + archiver.getEndDateString() + ".");
            List<SObject> attachments = archiver.getAttachments();

            Map<DoclinkImport, byte[]> docsWithBody;

            if (attachments.size() > 0) {
                System.out.println(attachments.size() + " attachments found! Generating XML structure for all attachments.");
                docsWithBody = archiver.getArchiveDocumentToBodyMap(attachments);

                System.out.println("Writing attachments to " + archiver.getTargetDirectory());
                archiver.writeFilesToDirectory(docsWithBody);

                // Freeing up memory
                attachments = null;
                docsWithBody = null;
                System.gc();
            } else {
                System.out.println("No attachments retrieved.");
            }

            System.out.println("Querying salesforce for all files from " + archiver.getStartDateString() + " to " + archiver.getEndDateString() + ".");
            List<SObject> files = archiver.getFiles();

            if (files.size() > 0) {
                System.out.println(files.size() + " files found! Generating XML structure for all files.");
                docsWithBody = archiver.getArchiveDocumentToBodyMap(files);

                System.out.println("Writing files to " + archiver.getTargetDirectory());
                archiver.writeFilesToDirectory(docsWithBody);
            } else {
                System.out.println("No files retrieved.");
            }

            System.out.println("Done!");
        } catch (Exception e) {
            System.out.println("Something happened! Here is what we know: " + e.getMessage() + "\n" + Arrays.toString(e.getStackTrace()));
        }
    }
}
